<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.jasig.cas.client.authentication.AttributePrincipal" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SSO Sample Web App</title>
    </head>
    <body>
 
    <h1>SSO Sample Web App</h1>
    <hr>
    
    <p>Hello, <%= request.getRemoteUser() %></p>
    <p><a href="logout.jsp" title="Click here to log out">Logout</a></p>

    </body>
</html>
